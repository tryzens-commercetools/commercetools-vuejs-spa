#!/bin/bash

echo "Stoping Nginx"
sudo systemctl stop nginx

echo "Stoping Node server"
pm2 stop all

pkill node